# 哪吒扩展板Mind+用户库


![哪吒扩展板](arduinoC/_images/nezha.png)

---------------------------------------------------------

## 目录

* [相关链接](#相关链接)
* [积木列表](#积木列表)
* [示例程序](#示例程序)
  * [Microbit](#Microbit)
  * [掌控板](#掌控板)
* [许可证](#许可证)
* [支持列表](#支持列表)
* [更新日志](#更新日志)


## 相关链接

* 本项目加载链接: ```https://gitee.com/hmilycheng/nezha```


## 积木列表

![积木列表](micropython/_images/%E7%A7%AF%E6%9C%A8.png)


## 示例程序

### Microbit

![示例程序](arduinoC/_images/%E7%A4%BA%E4%BE%8B%E7%A8%8B%E5%BA%8F.png)

### 掌控板

![示例程序](micropython/_images/%E7%A4%BA%E4%BE%8B%E7%A8%8B%E5%BA%8F.png)



## 许可证

MIT

## 支持列表

主板型号                | 实时模式    | ArduinoC   | MicroPython    | 备注
------------------ | :----------: | :----------: | :---------: | -----
micro:bit        |             |        √      |             |
掌控板        |             |              | √ |


## 更新日志
* V0.0.1  2022-01-21 基础功能完成
