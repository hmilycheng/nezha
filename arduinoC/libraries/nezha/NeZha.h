class NeZha {
 public:
  NeZha();
  
  void set_motor_speed(int motor, int speed);
  void stop_motor(int motor);
  void stop_all_motor(void);
  void set_servo_angle(int servotype, int servo, int angle);
  void set_servo_speed(int servo, int speed);
 private:
  void i2cWriteBuf(int addr, unsigned char *p, int len);
};