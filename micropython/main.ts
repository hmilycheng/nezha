//% color=#32b9b9 iconWidth=50 iconHeight=40
namespace nezha {
    //% block="set motor [motor] speed to [speed]%" blockType="command"
    //% speed.shadow="range" speed.params.min=-100 speed.params.max=100 speed.defl=100
    //% motor.shadow="dropdown" motor.options="MotorList"
    export function setMotorSpeed(parameter: any, block: any){
        let motor= parameter.motor.code;
        let speed = parameter.speed.code;
        Generator.addImport("from mpython import *");
        Generator.addImport("from nezha import *");		
        Generator.addInitHeader(`nezha`,`nezha = nezha(i2c)`); 	        
        Generator.addCode(`nezha.set_motor_speed("${motor}",${speed})`);
    }

	
	
    //% block="stop motor [motor]" blockType="command"
    //% motor.shadow="dropdown" motor.options="MotorList"
    export function stopMotor(parameter: any, block: any){
        let motor= parameter.motor.code;
        Generator.addImport("from mpython import *");
        Generator.addImport("from nezha import *");		
        Generator.addInitHeader(`nezha`,`nezha = nezha(i2c)`); 	 
        Generator.addCode(`nezha.stop_motor("${motor}")`);
    }
	
	
	

    //% block="Stop all motor" blockType="command"
    export function stopAllMotor(parameter: any, block: any){
        Generator.addCode(`nezha.stop_all_motor()`);
    }
	

	
    //% block="Set [servotype] servo [servo] angle to [angle]°" blockType="command"
    //% servotype.shadow="dropdown" servotype.options="ServoTypeList"
    //% servo.shadow="dropdown" servo.options="ServoList"
    //% angle.shadow="number" angle.defl="90"
    export function setServoAngel(parameter: any, block: any){
        let servotype = parameter.servotype.code;
        let servo = parameter.servo.code;
        let angle = parameter.angle.code;
        Generator.addImport("from mpython import *");
        Generator.addImport("from nezha import *");		
        Generator.addInitHeader(`nezha`,`nezha = nezha(i2c)`); 	
        Generator.addCode(`nezha.set_servo_angle(${servotype},${servo},${angle})`);
    }



    //% block="Set 360° servo [servo] speed to [speed]%" blockType="command"
    //% servo.shadow="dropdown" servo.options="ServoList" servo.defl="ServoList.S1"
    //% speed.shadow="range" speed.params.min=-100 speed.params.max=100 speed.defl=100
    export function setServoSpeed(parameter: any, block: any){
        let servo = parameter.servo.code;
        let speed = parameter.speed.code;
        Generator.addImport("from mpython import *");
        Generator.addImport("from nezha import *");		
        Generator.addInitHeader(`nezha`,`nezha = nezha(i2c)`); 	
        Generator.addCode(`nezha.set_servo_speed(${servo},${speed})`);
    }
	
	
}